-- local bot = BottomFPSFrame or CreateFrame("frame", "BottomFPSFrame", WorldFrame)
-- bot:RegisterAllEvents()
-- local top = TopFPSFrame or CreateFrame("frame", "TopFPSFrame")
-- bot:SetScript("OnEvent", function (this)
--     if bot.lock then return end
--     local time = GetTime()
--     if this.stime then
--         this.itime = time - this.stime
--     end
--     this.stime = time
--     if top.ltime then
--         this.ftime = time - top.ltime
--     end
--     bot.lock = true
-- end)
-- bot:SetScript("OnUpdate", function (this)
--     if bot.lock then bot.lock = nil return end
--     local time = GetTime()
--     if this.stime then
--         this.itime = time - this.stime
--     end
--     this.stime = time
--     if top.ltime then
--         this.ftime = time - top.ltime
--     end
-- end)
-- top:SetFrameStrata("TOOLTIP")
-- top:SetFrameLevel(200)
-- top:SetScript("OnUpdate", function (this)
--     if not TopFPSFrame.l1 then
--         TopFPSFrame.l1 = TopFPSFrame.l1 or EFrame.Label()
--         TopFPSFrame.l2 = TopFPSFrame.l2 or EFrame.Label()
--         TopFPSFrame.l3 = TopFPSFrame.l3 or EFrame.Label()
--         TopFPSFrame.l4 = TopFPSFrame.l4 or EFrame.Label()
--         TopFPSFrame.l1.anchorTopLeft = EFrame.root.topLeft
--         TopFPSFrame.l1.margins = 500
--         TopFPSFrame.l2.anchorTop = TopFPSFrame.l1.bottom
--         TopFPSFrame.l2.anchorLeft = TopFPSFrame.l1.left
--         TopFPSFrame.l3.anchorTop = TopFPSFrame.l2.bottom
--         TopFPSFrame.l3.anchorLeft = TopFPSFrame.l2.left
--         TopFPSFrame.l4.anchorTop = TopFPSFrame.l3.bottom
--         TopFPSFrame.l4.anchorLeft = TopFPSFrame.l3.left
--         TopFPSFrame.l1.anchorTopLeft = EFrame.root.topleft
--     end
--     bot.lock = nil
--     local time = GetTime()
--     local ltime = time - bot.stime
--     this.ntime = ltime
--     this.ltime = time
--     if not bot.ftime then return end
--     local ttime = bot.ftime + ltime
--     local a =  1 - .5/GetFramerate()
--     if  ltime > (this.peak or -1) then
--         this.peak = ltime
--         this.fpeak = ttime
--         top.l3.text = format("Lua peak:\nframe: %4d ms (%d FPS)\nlua: %4d ms", this.fpeak * 1000, math.ceil(1/this.fpeak-.5),  this.peak * 1000)
--         this.l3.opacity = 1
--     else
--         this.peak = this.peak * a
--         this.l3.opacity =  0.5 + (this.l3.opacity - 0.5) * a
--     end
--     if  ttime > (this.fpeakf or -1) then
--         this.peakf = ltime
--         this.fpeakf = ttime
--         top.l4.text = format("Frame peak:\nframe: %4d ms (%d FPS)\nlua: %4d ms", this.fpeakf * 1000, math.ceil(1/this.fpeakf-.5),  this.peakf * 1000)
--         this.l4.opacity = 1
--     else
--         this.fpeakf = this.fpeakf * a
--         this.l4.opacity = 0.5 + (this.l4.opacity - 0.5) * a
--     end
--     if ttime * 1000 > 25 then
--         debug(format("frame: %4d (%4d|%4d) ms", ttime * 1000, ltime*1000, bot.ftime * 1000))
--     end
--     top.l1.text = format("Frame: %4d ms", (ttime) * 1000)
--     top.l2.text = format("Lua:   %4d ms", (ltime) * 1000)
-- end)
function toString(a, loop)
  loop = loop or {}
    if a == nil then return "nil" end
    if a == true then return "TRUE" end
    if a == false then return "FALSE" end
    if type(a) == "string" then return "\""..a.."\"" end
    if type(a) == "table" then
        local address = tostring(a)
        if loop[address] then
            return address
        else
            loop[address] = true
        end
        local out = format("%s {", a.GetName and a:GetName() or address)
        local sep = ""
        for k,v in pairs(a) do
            out = out..sep..toString(k, loop)..": "..toString(v, loop)
            sep = ", "
        end
        return out.."}"
    end
    return tostring(a)
end

function debug(a)
  print(toString(a))
end

local errors = {}
EDebug = {recent = 0, frame = CreateFrame("frame")}
EDebug.errors = errors

EDebug.frame:SetScript("OnUpdate", function ()
    if EDebug.lastChat and GetTime() - EDebug.lastChat > 5 then
        if EDebug.recent > 3 then
            print(format("%d error%s suppressed in last 5 seconds.", EDebug.recent - 3, EDebug.recent == 1 and "" or "s"))
        end
        EDebug.recent = 0
    end
end)

function EDebug:prettyError(i)
    if i > #errors or i < 1 then
        return "Invalid error index", -1
    end
    local err = errors[i]
    return format("|cffff3333%s|r\n|cffff8800%s|r\n|cff00ff00EFrame:\n%s|r", err.msg, err.trace or "", err.eframe)
end

function EDebug:error(i)
    local err = errors[i]
    return format("%s\n%s\n%s", err.msg, err.trace or "", err.eframe)
end

local function timestamp()
    return date("%X %x")
end

local lastError = {}
seterrorhandler(function (msg)
    local time = GetTime()
    if not EDebug.lastChat or (time - EDebug.lastChat > 5) or EDebug.recent < 3 then
        print(msg)
        EDebug.lastChat = time
    end
    EDebug.recent = EDebug.recent +1
    local trace = debugstack(3,1000,1000)
    if msg == lastError.msg and trace == lastError.trace then
        lastError.count = lastError.count +1
        lastError.last = timestamp()
        if EDebug.window and EDebug.window.errno == #errors then
            EDebug.window.info.count = lastError.count
            EDebug.window.info.last = lastError.last
        end
        return
    end
    lastError = {msg = msg, trace = trace, count = 1, first = timestamp(), eframe = table.concat(EFrame.stack,"\n")}
    tinsert(errors, lastError)
    if EDebug.window then
        EDebug.window.numErrors = #errors
    end
end)
seterrorhandler = function() end

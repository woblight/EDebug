local EDWindow

local function UIInit()
    EDWindow = EFrame.Window()
    EDebug.window = EDWindow
    EDWindow.visible = false
    EDWindow.title = "EDebug"
    EDWindow:attach("errno")
    EDWindow:attach("numErrors")
    EDWindow.info = EFrame.Object(EDWindow)
    EDWindow.info:attach("count")
    EDWindow.info:attach("first")
    EDWindow.info:attach("last")
    EDWindow.info.count = 0
    EDWindow.info.first = ""
    EDWindow.info.last = ""
    EDWindow.onErrnoChanged = function ()
        local err = EDebug.errors[EDWindow.errno]
        if err then
            EDWindow.info.count = err.count
            EDWindow.info.first = err.first
            EDWindow.info.last = err.last or ""
        end
    end
    EDWindow.errno = EFrame.bind(EDWindow, "numErrors")
    EDWindow.numErrors = getn(EDebug.errors)
    EDWindow.centralItem = EFrame.ColumnLayout(EDWindow)
    EDWindow.centralItem.spacing = 2
    local brow = EFrame.RowLayout(EDWindow.centralItem)
    local prevbtn = EFrame.Button(brow)
    brow.spacing = 2
    prevbtn.text = "Prev."
    prevbtn.onClicked = function() EDWindow.errno = EDWindow.errno -1 end
    prevbtn.enabled = EFrame.bind(function() return EDWindow.errno > 1 end)
    local errnoLabel = EFrame.Label(brow)
    errnoLabel.text = EFrame.bind(function() return format("%d/%d", EDWindow.errno, EDWindow.numErrors) end)
    errnoLabel.visible = EFrame.bind(function() return EDWindow.numErrors > 0 end)
    local nextbtn = EFrame.Button(brow)
    nextbtn.text = "Next"
    nextbtn.onClicked = function() EDWindow.errno = EDWindow.errno +1 end
    nextbtn.enabled = EFrame.bind(function() return EDWindow.numErrors ~= 0 and EDWindow.errno ~= EDWindow.numErrors end)
    local inforow = EFrame.RowLayout(EDWindow.centralItem)
    inforow.visible = EFrame.bind(function() return EDWindow.numErrors > 0 end)
    inforow.spacing = 4
    local count = EFrame.Label(inforow)
    count.text = EFrame.bind(function() return format("Count: %d", EDWindow.info.count) end)
    local first = EFrame.Label(inforow)
    first.text = EFrame.bind(function() return format("First: %s", EDWindow.info.first) end)
    local last = EFrame.Label(inforow)
    last.text = EFrame.bind(function() return format("Last: %s", EDWindow.info.last) end)
    local sall = EFrame.Button(EDWindow.centralItem)
    sall.text = "Select All"
    tarea = EFrame.TextArea(EDWindow.centralItem)
    sall.onClicked = function() tarea:selectText() end
    tarea.text = EFrame.bind(function() return EDWindow.numErrors > 0 and EDebug:prettyError(EDWindow.errno) or "No error!" end)
    tarea.implicitWidth=600
    tarea.implicitHeight=800
    tarea.Layout.fillWidth = true
    tarea.Layout.fillHeight = true
    tarea.readOnly = true
    function tarea:onFocusChanged(f) if f then self.keyboardEnabled = true end self.n_text:EnableKeyboard(f and 0 or 1) end
    function tarea:onKeyPressed(k)
        if k == "CTRL" then
            self.n_text:EnableKeyboard(1)
        elseif k == "ESCAPE" then
            self.keyboardEnabled = false
            self.focus = false
            EDWindow.visible = false
        end
    end
    function tarea:onKeyReleased(k) if k == "CTRL" then self.n_text:EnableKeyboard(0) end end
    EDWindow.onVisibleChanged = function(self) if not self.visible then self:deleteLater() EDWindow = nil EDebug.window = nil end end
end


local function chatcmd(msg)
    if not tonumber(msg) then
        local save = tonumber(strmatch(msg,"save (%d+)"))
        if save then
            _G.EDebugDumps = EDebugDumps or {}
            tinsert(EDebugDumps, EDebug:error(save))
        else
            if not EDWindow then
                UIInit()
            end
            EDWindow.visible = not EDWindow.visible
        end
    else
        local err = EDebug:prettyError(tonumber(msg))
        for str in string.gmatch(err, "[^\n]*") do
            print(str)
        end
    end
end

SLASH_EDEBUG1 = "/errors"
SLASH_EDEBUG2 = "/err"
SlashCmdList["EDEBUG"] = chatcmd
